# Awesome Project Build with TypeORM

Steps to run this project:

1. Criar Base de Dados (CONFIGURACOES no arquivo ormconfig.json)

2. Run yarn install

3. Run yarn build

4. Run yarn typeorm migration:run

5. Run yarn start

6. Importar o arquivo run-dump.sql no banco postgres

7. Abrir no navegador http://localhost:3333 ou http://127.0.0.1:3333

8. Importar o arquivo insomnia.json no app Insomnia para HOMOLOGAR o Back-end
