INSERT INTO nivel_acesso (id_nivel_acesso, "role")
VALUES(2, 'APP');
INSERT INTO nivel_acesso (id_nivel_acesso, "role")
VALUES(3, 'PARCEIRO');
INSERT INTO endereco (
        id_endereco,
        cep,
        logradouro,
        complemento,
        bairro,
        localidade,
        uf
    )
VALUES(
        8,
        '31255620',
        'R Libero Badaro',
        '394, Apto 101',
        'Jaraguá',
        'Belo Horizonte',
        'MG'
    );
INSERT INTO endereco (
        id_endereco,
        cep,
        logradouro,
        complemento,
        bairro,
        localidade,
        uf
    )
VALUES(
        9,
        '30250330',
        'R Libero Badaro',
        '394, Apto 101',
        'Jaraguá',
        'Belo Horizonte',
        'MG'
    );
INSERT INTO usuario_app (
        id_usuario,
        cpf_usuario,
        celular_usuario,
        "password",
        criado_em,
        atualizado_em
    )
VALUES(
        20,
        '05380325610',
        '31988776650',
        '$2b$10$.1BQHHcJssNOM4DPrB9XZeLTGQRUEdaZp/.lpKGIDpYBW.h3bcpzi',
        '2020-10-01',
        '2020-10-01'
    );
INSERT INTO usuario_app (
        id_usuario,
        cpf_usuario,
        celular_usuario,
        "password",
        criado_em,
        atualizado_em
    )
VALUES(
        27,
        '12345678904',
        '31984106645',
        '$2b$10$oVuLiBtJSDvajiKUXxR7ge0o0b0n1SZp/UDVE/N8.TqKgF9r.1vKK',
        '2020-10-02',
        '2020-10-02'
    );
INSERT INTO cliente (
        id_cliente,
        fk_id_usuario,
        fk_id_endereco,
        nome,
        cpf_usuario,
        rg_usuario,
        nascimento,
        genero,
        celular_usuario,
        email,
        criado_em,
        atualizado_em,
        cep,
        logradouro,
        complemento,
        bairro,
        cidade,
        uf
    )
VALUES(
        17,
        20,
        9,
        'Marco Túlio Rocha Carvalho',
        '05380325610',
        'MG11084680',
        '1980-11-23',
        'M',
        '31988776650',
        'v8rocha@gmail.com',
        '2020-10-01',
        '2020-10-02',
        '30250330',
        'R Libero Badaro',
        '111',
        'Jaraguá',
        'Belo Horizonte',
        'MG'
    );
INSERT INTO cliente (
        id_cliente,
        fk_id_usuario,
        fk_id_endereco,
        nome,
        cpf_usuario,
        rg_usuario,
        nascimento,
        genero,
        celular_usuario,
        email,
        criado_em,
        atualizado_em,
        cep,
        logradouro,
        complemento,
        bairro,
        cidade,
        uf
    )
VALUES(
        21,
        27,
        NULL,
        'Roberto Lui',
        '12345678904',
        NULL,
        NULL,
        NULL,
        '31984106645',
        'roberto@gmail.com',
        '2020-10-02',
        '2020-10-02',
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL
    );
INSERT INTO usuario_app_nivel_acesso (fk_id_usuario_app, fk_id_nivel_acesso)
VALUES(20, 2);
INSERT INTO usuario_app_nivel_acesso (fk_id_usuario_app, fk_id_nivel_acesso)
VALUES(27, 2);
INSERT INTO usuario_parceiro (
        id_usuario_parceiro,
        cnpj,
        "password",
        criado_em,
        atualizado_em
    )
VALUES(
        6,
        '11302115000151',
        '$2b$10$LwhJRZAsAMIdweZs11/Z2u5ZqK84RBcDU8eg9UzU4iUo/uSlPq3oK',
        '2020-10-01',
        '2020-10-01'
    );
INSERT INTO parceiro (
        id_parceiro,
        fk_id_usuario_parceiro,
        fk_id_endereco,
        razao_social,
        nome_fantasia,
        telefone_contato_1,
        telefone_contato_2,
        telefone_contato_3,
        telefone_contato_4,
        email_1,
        email_2,
        email_3,
        email_4,
        pessoa_contato,
        celular_contato,
        email_contato,
        criado_em,
        atualizado_em,
        latitude,
        longitude,
        cep,
        logradouro,
        complemento,
        bairro,
        cidade,
        uf
    )
VALUES(
        6,
        6,
        9,
        'Supermercados BH',
        'Supermercados BH',
        '3198877661',
        '3198877662',
        '3198877663',
        '3198877664',
        'a@gmail.com',
        'a@gmail.com',
        'a@gmail.com',
        'a@gmail.com',
        'Arthur Goncalves',
        '31988776655',
        'contato@gmail.com',
        '2020-10-01',
        '2020-10-01',
        '-19.8620142',
        '43.9480646',
        '31250330',
        'R. Libero Badaró',
        '101',
        'Jaragua',
        'Belo Horizonte',
        'MG'
    );
INSERT INTO parceiro (
        id_parceiro,
        fk_id_usuario_parceiro,
        fk_id_endereco,
        razao_social,
        nome_fantasia,
        telefone_contato_1,
        telefone_contato_2,
        telefone_contato_3,
        telefone_contato_4,
        email_1,
        email_2,
        email_3,
        email_4,
        pessoa_contato,
        celular_contato,
        email_contato,
        criado_em,
        atualizado_em,
        latitude,
        longitude,
        cep,
        logradouro,
        complemento,
        bairro,
        cidade,
        uf
    )
VALUES(
        7,
        6,
        9,
        'EPA Plus',
        'EPA Plus',
        '3198877661',
        '3198877662',
        '3198877663',
        '3198877664',
        'a@gmail.com',
        'a@gmail.com',
        'a@gmail.com',
        'a@gmail.com',
        'Arthur Goncalves',
        '31988776655',
        'contato@gmail.com',
        '2020-10-01',
        '2020-10-01',
        '-19.8620142',
        '43.9480646',
        '30250330',
        'Av Pinheiros',
        '99',
        'Centro',
        'Sao Paulo',
        'SP'
    );
INSERT INTO usuario_parceiro_nivel_acesso (fk_id_usuario_parceiro, fk_id_nivel_acesso)
VALUES(6, 3);
INSERT INTO titulo (
        id_titulo,
        fk_id_parceiro,
        numero_titulo,
        data_lancamento,
        data_vencimento
    )
VALUES(7, 6, '1122334455', '2020-12-11', '2020-12-04');
INSERT INTO titulo (
        id_titulo,
        fk_id_parceiro,
        numero_titulo,
        data_lancamento,
        data_vencimento
    )
VALUES(6, 6, '1321231231', '2020-11-09', '2020-12-04');
INSERT INTO registro_titulo (
        id_registro_titulo,
        fk_id_usuario,
        fk_id_titulo,
        data_registro,
        ganhador
    )
VALUES(3, 20, 6, '2020-09-19', 'N');
INSERT INTO registro_titulo (
        id_registro_titulo,
        fk_id_usuario,
        fk_id_titulo,
        data_registro,
        ganhador
    )
VALUES(4, 20, 6, '2020-09-22', 'S');