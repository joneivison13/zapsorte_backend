import { Request, Response } from "express";
import EnderecoService from "../services/EnderecoService";

class EnderecoController {
  async getByCep(request: Request, response: Response) {
    return EnderecoService.getByCep(request, response);
  }

  async store(request: Request, response: Response) {
    return EnderecoService.store(request, response);
  }

  async get(request: Request, response: Response) {
    return EnderecoService.get(request, response);
  }
}

export default new EnderecoController();
