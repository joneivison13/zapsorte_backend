import { Request, Response } from 'express'
import LoginUsuarioService from '../services/LoginUsuarioService'

class LoginUsuarioController {
  async store (request: Request, response: Response) {
    try {
      return LoginUsuarioService.store(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async update (request: Request, response: Response) {
    try {
      return LoginUsuarioService.update(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async getByCelular (request: Request, response: Response) {
    try {
      return LoginUsuarioService.getByCelular(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async getByCPF (request: Request, response: Response) {
    try {
      return LoginUsuarioService.getByCPF(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async upload (request: Request, response: Response) {
    try {
      return LoginUsuarioService.upload(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new LoginUsuarioController()
