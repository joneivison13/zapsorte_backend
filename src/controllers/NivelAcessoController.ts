import { Request, Response } from 'express'
import NivelAcessoService from '../services/NivelAcessoService'

class NivelAcessoController {
  async store (request: Request, response: Response) {
    try {
      return NivelAcessoService.store(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async update (request: Request, response: Response) {
    try {
      return NivelAcessoService.update(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async get (request: Request, response: Response) {
    try {
      return NivelAcessoService.get(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async delete (request: Request, response: Response) {
    try {
      return NivelAcessoService.delete(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new NivelAcessoController()
