import { Request, Response } from 'express'
import SessionService from '../services/SessionService'

class SessionController {
  async store (request: Request, response: Response) {
    return SessionService.store(request, response)
  }

  async storeParceiro (request: Request, response: Response) {
    return SessionService.storeParceiro(request, response)
  }

  async updateLoginUsuario (request: Request, response: Response) {
    return SessionService.updatePassApp(request, response)
  }

  async updateUsuarioParceiro (request: Request, response: Response) {
    return SessionService.updatePassParceiro(request, response)
  }

  async sessionRecuperarSenha (request: Request, response: Response) {
    return SessionService.sessionRecuperarSenha(request, response)
  }
}

export default new SessionController()
