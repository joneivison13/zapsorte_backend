import { Request, Response } from 'express'
import TransferenciaService from '../services/TransferenciaService'

class TransferenciaController {
  async store (request: Request, response: Response) {
    try {
      return TransferenciaService.store(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new TransferenciaController()
