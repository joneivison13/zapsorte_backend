import { Request, Response } from 'express'
import UsuarioPinService from '../services/UsuarioPinService'

class UsuarioPinController {
  async generate (request: Request, response: Response) {
    try {
      return UsuarioPinService.generate(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async validate (request: Request, response: Response) {
    try {
      return UsuarioPinService.validate(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }

  async generateRecuperarSenha(request: Request, response: Response){
    try {
      return UsuarioPinService.generateRecuperarSenha(request, response)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new UsuarioPinController()
