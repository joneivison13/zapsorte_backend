import { MigrationInterface, QueryRunner, Table } from 'typeorm'

export class createTableLoginUsuario1599684991921 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    const table = new Table({
      name: 'login_usuario',
      columns: [
        {
          name: 'id_login_usuario',
          type: 'int',
          isPrimary: true,
          isNullable: false,
          isGenerated: true,
          generationStrategy: 'increment'
        }, {
          name: 'avatar',
          type: 'varchar',
          isNullable: true,
        }, {
          name: 'cpf',
          type: 'varchar',
          isNullable: true,
          length: '11'
        }, {
          name: 'numero_celular',
          type: 'varchar',
          isNullable: true,
          length: '11'
        }, {
          name: 'password_usuario',
          type: 'varchar',
          isNullable: false,
          length: '255'
        },{
          name: 'ativado',
          type: 'boolean',
          isNullable: false
        },{
          name: 'saldo_atual',
          type: 'decimal',
          isNullable: false,
          precision: 11,
          scale: 2,
          default: '0'
        },{
          name: 'saldo_pedagio',
          type: 'decimal',
          isNullable: false,
          precision: 11,
          scale: 2,
          default: '0'
        },{
          name: 'email',
          type: 'varchar',
          isNullable: true,
          length: '100'
        }, {
          name: 'criado_em',
          type: 'date',
          isNullable: false,
          default: 'now()'
        }, {
          name: 'atualizado_em',
          type: 'date',
          default: 'now()'
        }
      ]
    })

    await queryRunner.createTable(table, true)
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('login_usuario')
  }
}
