import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class createTableUsuarioPin1599685257485 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    const table = new Table({
      name: 'usuario_pin',
      columns: [
        {
          name: 'id_usuario_pin',
          type: 'int',
          isPrimary: true,
          isNullable: false,
          isGenerated: true,
          generationStrategy: 'increment'
        }, {
          name: 'fk_id_login_usuario',
          type: 'int',
          isNullable: true
        }, {
          name: 'pin_usuario',
          type: 'varchar',
          isNullable: false
        }, {
          name: 'celular',
          type: 'varchar',
          length: '11',
          isNullable: false
        }, {
          name: 'criado_em',
          type: 'date',
          isNullable: false,
          default: 'now()'
        }, {
          name: 'atualizado_em',
          type: 'date',
          default: 'now()'
        }
      ]
    })

    await queryRunner.createTable(table, true)

    const fkUser = new TableForeignKey({
      columnNames: ['fk_id_login_usuario'],
      referencedColumnNames: ['id_login_usuario'],
      referencedTableName: 'login_usuario',
      onDelete: 'CASCADE'
    })

    await queryRunner.createForeignKey('usuario_pin', fkUser)
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('usuario_pin')
  }
}
