import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class createTableUsuarioAppNivelAcesso1599687714643 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    const table = new Table({
      name: 'usuario_app_nivel_acesso',
      columns: [{
        name: 'fk_id_login_usuario',
        type: 'int',
        isNullable: false
      }, {
        name: 'fk_id_nivel_acesso',
        type: 'int',
        isNullable: false
      }]
    })

    await queryRunner.createTable(table)

    const fkUsuarioApp = new TableForeignKey({
      columnNames: ['fk_id_login_usuario'],
      referencedColumnNames: ['id_login_usuario'],
      referencedTableName: 'login_usuario',
      onDelete: 'CASCADE'
    })

    const fkNivelAcesso = new TableForeignKey({
      columnNames: ['fk_id_nivel_acesso'],
      referencedColumnNames: ['id_nivel_acesso'],
      referencedTableName: 'nivel_acesso',
      onDelete: 'CASCADE'
    })

    await queryRunner.createForeignKeys('usuario_app_nivel_acesso', [fkUsuarioApp, fkNivelAcesso])
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('usuario_app_nivel_acesso')
  }
}
