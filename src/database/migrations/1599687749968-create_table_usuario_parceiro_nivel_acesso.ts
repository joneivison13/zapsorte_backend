import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class createTableUsuarioParceiroNivelAcesso1599687749968 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    const table = new Table({
      name: 'usuario_parceiro_nivel_acesso',
      columns: [{
        name: 'fk_id_usuario_parceiro',
        type: 'int',
        isNullable: false
      }, {
        name: 'fk_id_nivel_acesso',
        type: 'int',
        isNullable: false
      }]
    })

    await queryRunner.createTable(table)

    const fkUsuarioApp = new TableForeignKey({
      columnNames: ['fk_id_usuario_parceiro'],
      referencedColumnNames: ['id_usuario_parceiro'],
      referencedTableName: 'usuario_parceiro',
      onDelete: 'CASCADE'
    })

    const fkNivelAcesso = new TableForeignKey({
      columnNames: ['fk_id_nivel_acesso'],
      referencedColumnNames: ['id_nivel_acesso'],
      referencedTableName: 'nivel_acesso',
      onDelete: 'CASCADE'
    })

    await queryRunner.createForeignKeys('usuario_parceiro_nivel_acesso', [fkUsuarioApp, fkNivelAcesso])
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('usuario_parceiro_nivel_acesso')
  }
}
