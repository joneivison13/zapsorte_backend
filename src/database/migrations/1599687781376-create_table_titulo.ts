import { MigrationInterface, QueryRunner, Table, TableForeignKey } from 'typeorm'

export class createTableTitulo1599687781376 implements MigrationInterface {
  public async up (queryRunner: QueryRunner): Promise<void> {
    const table = new Table({
      name: 'titulo',
      columns: [{
        name: 'id_titulo',
        type: 'int',
        isPrimary: true,
        isNullable: false,
        isGenerated: true,
        generationStrategy: 'increment'
      }, {
        name: 'fk_id_parceiro',
        type: 'int',
        isNullable: false
      }, {
        name: 'numero_titulo',
        type: 'varchar',
        length: '400',
        isNullable: false
      }, {
        name: 'data_lancamento',
        type: 'date',
        isNullable: false
      }, {
        name: 'data_vencimento',
        type: 'date',
        isNullable: false
      }]
    })

    await queryRunner.createTable(table)

    const fkUser = new TableForeignKey({
      columnNames: ['fk_id_parceiro'],
      referencedColumnNames: ['id_parceiro'],
      referencedTableName: 'parceiro',
      onDelete: 'CASCADE'
    })

    await queryRunner.createForeignKey('titulo', fkUser)
  }

  public async down (queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('titulo')
  }
}
