import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createRecargaCelularLoginUsuario1611496400833 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'recarga_celular_login_usuario',
            columns: [
            {
                name: 'fk_id_login_usuario',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_id_recarga_celular',
                type: 'int',
                isNullable: false
              }, {
                name: 'criado_em',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }, {
                name: 'atualizado_em',
                type: 'date',
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fkLoginUsuario = new TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
          })
      
          const fkRecargCelular = new TableForeignKey({
            columnNames: ['fk_id_recarga_celular'],
            referencedColumnNames: ['id_recarga_celular'],
            referencedTableName: 'recarga_celular',
            onDelete: 'CASCADE'
          })
      
        await queryRunner.createForeignKeys('recarga_celular_login_usuario', [fkLoginUsuario, fkRecargCelular])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.dropTable('recarga_celular_login_usuario')
    }

}
