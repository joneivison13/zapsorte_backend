import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createParceiroEndereco1611496746464 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'parceiro_endereco',
            columns: [
              {
                name: 'fk_id_parceiro',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_id_endereco',
                type: 'int',
                isNullable: false
              }, {
                name: 'complemento',
                type: 'varchar',
                isNullable: false
              }, {
                name: 'latitude',
                type: 'varchar',
                length: '100',
                isNullable: true
              }, {
                name: 'longitude',
                type: 'varchar',
                length: '100',
                isNullable: true
              }, {
                name: 'criado_em',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }, {
                name: 'atualizado_em',
                type: 'date',
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fkParceiro = new TableForeignKey({
            columnNames: ['fk_id_parceiro'],
            referencedColumnNames: ['id_parceiro'],
            referencedTableName: 'parceiro',
            onDelete: 'CASCADE'
          })
      
          const fkEndereco = new TableForeignKey({
            columnNames: ['fk_id_endereco'],
            referencedColumnNames: ['id_endereco'],
            referencedTableName: 'endereco',
            onDelete: 'CASCADE'
          })
      
          await queryRunner.createForeignKeys('parceiro_endereco', [fkParceiro, fkEndereco])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('parceiro_endereco')
    }

}
