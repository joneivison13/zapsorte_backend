import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createUsuarioParceiroParceiro1611497083535 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'usuario_parceiro_parceiro',
            columns: [
            {
                name: 'fk_id_usuario_parceiro',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_id_parceiro',
                type: 'int',
                isNullable: false
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fk1 = new TableForeignKey({
            columnNames: ['fk_id_usuario_parceiro'],
            referencedColumnNames: ['id_usuario_parceiro'],
            referencedTableName: 'usuario_parceiro',
            onDelete: 'CASCADE'
          })
      
          const fk2 = new TableForeignKey({
            columnNames: ['fk_id_parceiro'],
            referencedColumnNames: ['id_parceiro'],
            referencedTableName: 'parceiro',
            onDelete: 'CASCADE'
          })
      
        await queryRunner.createForeignKeys('usuario_parceiro_parceiro', [fk1, fk2])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
      await queryRunner.dropTable('usuario_parceiro_parceiro')
    }

}
