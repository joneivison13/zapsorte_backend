import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createCaixaParceiro1611497354677 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'caixa_parceiro',
            columns: [{
                name: 'id_caixa_parceiro',
                type: 'int',
                isPrimary: true,
                isNullable: false,
                isGenerated: true,
                generationStrategy: 'increment'
              }, {
                name: 'fk_id_parceiro',
                type: 'int',
                isNullable: false
              }, {
                name: 'identificador_caixa_parceiro',
                type: 'varchar',
                isNullable: false
              }, {
                name: 'ativo',
                type: 'boolean',
                default: true,
                isNullable: false
              }, {
                name: 'criado_em',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }, {
                name: 'atualizado_em',
                type: 'date',
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fk = new TableForeignKey({
            columnNames: ['fk_id_parceiro'],
            referencedColumnNames: ['id_parceiro'],
            referencedTableName: 'parceiro',
            onDelete: 'CASCADE'
          })
      
          await queryRunner.createForeignKeys('caixa_parceiro', [fk])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('caixa_parceiro')
    }

}
