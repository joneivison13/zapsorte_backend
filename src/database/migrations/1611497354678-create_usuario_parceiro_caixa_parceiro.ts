import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createUsuarioParceiroCaixaParceiro1611497354678 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'usuario_parceiro_caixa_parceiro',
            columns: [
            {
                name: 'fk_id_nivel_acesso',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_id_caixa_parceiro',
                type: 'int',
                isNullable: false
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fk1 = new TableForeignKey({
            columnNames: ['fk_id_nivel_acesso'],
            referencedColumnNames: ['id_nivel_acesso'],
            referencedTableName: 'nivel_acesso',
            onDelete: 'CASCADE'
          })
      
          const fk2 = new TableForeignKey({
            columnNames: ['fk_id_caixa_parceiro'],
            referencedColumnNames: ['id_caixa_parceiro'],
            referencedTableName: 'caixa_parceiro',
            onDelete: 'CASCADE'
          })
      
        await queryRunner.createForeignKeys('usuario_parceiro_caixa_parceiro', [fk1, fk2])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('usuario_parceiro_caixa_parceiro')
    }

}
