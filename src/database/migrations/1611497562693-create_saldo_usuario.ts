import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createSaldoUsuario1611497562693 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'saldo_usuario',
            columns: [{
                name: 'id_saldo_usuario',
                type: 'int',
                isPrimary: true,
                isNullable: false,
                isGenerated: true,
                generationStrategy: 'increment'
              }, {
                name: 'fk_id_login_usuario',
                type: 'int',
                isNullable: false
              }, {
                name: 'valor_lancado',
                type: 'decimal',
                precision: 11,
                scale: 2,
                isNullable: false
              }, {
                name: 'data_lancamento_saldo_usuario',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }, {
                name: 'criado_em',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }, {
                name: 'atualizado_em',
                type: 'date',
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fk = new TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
          })
      
          await queryRunner.createForeignKeys('saldo_usuario', [fk])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('saldo_usuario')
    }

}
