import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createIndicacao1611497576037 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'indicacao',
            columns: [{
                name: 'fk_indicador',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_indicado',
                type: 'int',
                isNullable: false
              }, {
                name: 'criado_em',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }, {
                name: 'atualizado_em',
                type: 'date',
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fk1 = new TableForeignKey({
            columnNames: ['fk_indicador'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
          })

          const fk2 = new TableForeignKey({
            columnNames: ['fk_indicado'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
          })
      
          await queryRunner.createForeignKeys('indicacao', [fk1, fk2])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('indicacao')
    }

}
