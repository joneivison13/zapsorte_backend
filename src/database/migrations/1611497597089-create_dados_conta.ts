import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createDadosConta1611497597089 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'dados_conta',
            columns: [{
                name: 'id_dados_conta',
                type: 'int',
                isPrimary: true,
                isNullable: false,
                isGenerated: true,
                generationStrategy: 'increment'
              }, {
                name: 'fk_id_login_usuario',
                type: 'int',
                isNullable: false
              }, {
                name: 'agencia',
                type: 'varchar',
                isNullable: false
              }, {
                name: 'conta',
                type: 'varchar',
                isNullable: false
              }, {
                name: 'dvconta',
                type: 'varchar',
                isNullable: false
              }, {
                name: 'criado_em',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }, {
                name: 'atualizado_em',
                type: 'date',
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fk = new TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
          })
      
          await queryRunner.createForeignKeys('dados_conta', [fk])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('dados_conta')
    }

}
