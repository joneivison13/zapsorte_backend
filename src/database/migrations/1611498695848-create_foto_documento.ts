import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createFotoDocumento1611498695848 implements MigrationInterface {

    public async up (queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
          name: 'foto_documento',
          columns: [{
            name: 'id_foto_documento',
            type: 'int',
            isPrimary: true,
            isNullable: false,
            isGenerated: true,
            generationStrategy: 'increment'
          }, {
            name: 'endereco_imagem',
            type: 'varchar',
            isNullable: false
          }, {
            name: 'adicionado_em',
            type: 'date',
            isNullable: false,
            default: 'now()',
          }]
        })
    
        await queryRunner.createTable(table)
      }
    
      public async down (queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('foto_documento')
      }

}
