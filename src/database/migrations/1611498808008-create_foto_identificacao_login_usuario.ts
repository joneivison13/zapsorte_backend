import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createFotoIdentificacaoLoginUsuario1611498808008 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'foto_identificacao_login_usuario',
            columns: [{
                name: 'fk_id_foto_documento',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_id_login_usuario',
                type: 'int',
                isNullable: false
              }, {
                name: 'criado_em',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }, {
                name: 'atualizado_em',
                type: 'date',
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fk1 = new TableForeignKey({
            columnNames: ['fk_id_foto_documento'],
            referencedColumnNames: ['id_foto_documento'],
            referencedTableName: 'foto_documento',
            onDelete: 'CASCADE'
          })

          const fk2 = new TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
          })
      
          await queryRunner.createForeignKeys('foto_identificacao_login_usuario', [fk1, fk2])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('foto_identificacao_login_usuario')
    }

}
