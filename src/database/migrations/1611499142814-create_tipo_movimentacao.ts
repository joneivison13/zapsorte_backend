import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class createTipoMovimentacao1611499142814 implements MigrationInterface {

    public async up (queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
          name: 'tipo_movimentacao',
          columns: [{
            name: 'id_tipo_movimentacao',
            type: 'int',
            isPrimary: true,
            isNullable: false,
            isGenerated: true,
            generationStrategy: 'increment'
          }, {
            name: 'descricao_tipo_movimentacao',
            type: 'varchar',
            isNullable: false
          }]
        })
    
        await queryRunner.createTable(table)
      }
    
      public async down (queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('tipo_movimentacao')
      }

}
