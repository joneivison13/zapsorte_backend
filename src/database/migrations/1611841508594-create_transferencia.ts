import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createTransferencia1611841508594 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'transferencia',
            columns: [{
                name: 'id_transferencia',
                type: 'int',
                isPrimary: true,
                isNullable: false,
                isGenerated: true,
                generationStrategy: 'increment'
              }, {
                name: 'fk_id_banco',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_id_login_usuario',
                type: 'int',
                isNullable: false
              }, {
                name: 'fk_id_movimentacao_usuarios',
                type: 'int',
                isNullable: false
              }, {
                name: 'conta_corrente',
                type: 'varchar',
                isNullable: false
              }, {
                name: 'agencia',
                type: 'varchar',
                isNullable: false
              }, {
                name: 'cpf_destino',
                type: 'varchar',
                isNullable: false
              }, {
                name: 'nome_completo_destino',
                type: 'varchar',
                isNullable: false
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
    
          const fk1 = new TableForeignKey({
            columnNames: ['fk_id_banco'],
            referencedColumnNames: ['id_banco'],
            referencedTableName: 'banco',
            onDelete: 'CASCADE'
          })

          const fk2 = new TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
          })

          const fk3 = new TableForeignKey({
            columnNames: ['fk_id_movimentacao_usuarios'],
            referencedColumnNames: ['id_movimentacao_usuarios'],
            referencedTableName: 'movimentacao_usuarios',
            onDelete: 'CASCADE'
          })
      
          await queryRunner.createForeignKeys('transferencia', [fk1, fk2, fk3])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('transferencia')
    }

}
