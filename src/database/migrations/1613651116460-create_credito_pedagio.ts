import {MigrationInterface, QueryRunner, Table, TableForeignKey} from "typeorm";

export class createCreditoPedagio1613651116460 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        const table = new Table({
            name: 'credito_pedagio',
            columns: [{
                name: 'id_credito_pedagio',
                type: 'int',
                isPrimary: true,
                isNullable: false,
                isGenerated: true,
                generationStrategy: 'increment'
              }, {
                name: 'fk_id_login_usuario',
                type: 'int',
                isNullable: false
              }, {
                name: 'valor_credito_pedagio',
                type: 'decimal',
                precision: 11,
                scale: 2,
                isNullable: false
              }, {
                name: 'criado_em',
                type: 'date',
                isNullable: false,
                default: 'now()'
              }, {
                name: 'atualizado_em',
                type: 'date',
                default: 'now()'
              }
            ]
          })
      
          await queryRunner.createTable(table, true)
      
          const fk = new TableForeignKey({
            columnNames: ['fk_id_login_usuario'],
            referencedColumnNames: ['id_login_usuario'],
            referencedTableName: 'login_usuario',
            onDelete: 'CASCADE'
          })
      
          await queryRunner.createForeignKeys('credito_pedagio', [fk])
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable('credito_pedagio')
    }

}
