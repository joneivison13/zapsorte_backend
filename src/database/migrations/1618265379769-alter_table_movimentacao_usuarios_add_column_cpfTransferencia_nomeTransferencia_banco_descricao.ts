import {MigrationInterface, QueryRunner} from "typeorm";

export class alterTableMovimentacaoUsuariosAddColumnCpfTransferenciaNomeTransferenciaBancoDescricao1618265379769 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "movimentacao_usuarios" ADD COLUMN "fk_banco" integer`);
        await queryRunner.query(`ALTER TABLE "movimentacao_usuarios" ADD CONSTRAINT fk_movusuarios_banco FOREIGN KEY (fk_banco) REFERENCES banco(id_banco)`);
        await queryRunner.query(`ALTER TABLE "movimentacao_usuarios" ADD COLUMN "agencia" varchar(10)`);
        await queryRunner.query(`ALTER TABLE "movimentacao_usuarios" ADD COLUMN "conta_corrente" varchar(20)`);
        await queryRunner.query(`ALTER TABLE "movimentacao_usuarios" ADD COLUMN "cpf_transferencia" varchar(11)`);
        await queryRunner.query(`ALTER TABLE "movimentacao_usuarios" ADD COLUMN "nome_transferencia" varchar(200)`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "movimentacao_usuarios" DROP COLUMN "cpf_transferencia"`);
        await queryRunner.query(`ALTER TABLE "movimentacao_usuarios" DROP COLUMN "nome_transferencia"`);
        await queryRunner.query(`ALTER TABLE "movimentacao_usuarios" DROP COLUMN "agencia"`);
        await queryRunner.query(`ALTER TABLE "movimentacao_usuarios" DROP COLUMN "conta_corrente"`);
        await queryRunner.query(`ALTER TABLE "movimentacao_usuarios" DROP FOREIGN KEY fk_movusuarios_banco`);
        await queryRunner.query(`ALTER TABLE "movimentacao_usuarios" DROP COLUMN "fk_banco"`);
    }

}
