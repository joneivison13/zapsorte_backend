import { MigrationInterface, QueryRunner } from "typeorm";

export class adicionaFkIdPessoaEEmailEmUsuarioParceiro1626291428739
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "usuario_parceiro" ADD COLUMN "email" varchar(130)`
    );
    await queryRunner.query(
      `ALTER TABLE "usuario_parceiro" ADD COLUMN "fk_id_pessoa" integer`
    );
    await queryRunner.query(
      `ALTER TABLE "usuario_parceiro" ADD CONSTRAINT fk_id_pessoa FOREIGN KEY (fk_id_pessoa) REFERENCES pessoa(id_pessoa)`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "usuario_parceiro" DROP COLUMN "email" varchar(130)`
    );
    await queryRunner.query(
      `ALTER TABLE "usuario_parceiro" DROP COLUMN "fk_id_pessoa" integer`
    );
    await queryRunner.query(
      `ALTER TABLE "usuario_parceiro" DROP CONSTRAINT fk_id_pessoa FOREIGN KEY fk_id_pessoa`
    );
  }
}
