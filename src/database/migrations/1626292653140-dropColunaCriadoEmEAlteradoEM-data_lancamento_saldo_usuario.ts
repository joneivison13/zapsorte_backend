import { MigrationInterface, QueryRunner } from "typeorm";

export class dropColunaCriadoEmEAlteradoEMDataLancamentoSaldoUsuario1626292653140
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "saldo_usuario" DROP COLUMN criado_em`
    );
    await queryRunner.query(
      `ALTER TABLE "saldo_usuario" DROP COLUMN atualizado_em`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "saldo_usuario" ADD COLUMN criado_em timestamps`
    );
    await queryRunner.query(
      `ALTER TABLE "saldo_usuario" ADD COLUMN atualizado_em timestamps`
    );
  }
}
