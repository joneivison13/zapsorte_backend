import { MigrationInterface, QueryRunner } from "typeorm";

export class alteraTipoColunadataLancamentoSaldoUsuarioTimestamps1626293700361
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE saldo_usuario DROP data_lancamento_saldo_usuario`
    );
    await queryRunner.query(
      `ALTER TABLE saldo_usuario ADD data_lancamento_saldo_usuario timestamp DEFAULT now()`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
