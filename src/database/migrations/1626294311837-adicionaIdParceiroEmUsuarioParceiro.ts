import { MigrationInterface, QueryRunner } from "typeorm";

export class adicionaIdParceiroEmUsuarioParceiro1626294311837
  implements MigrationInterface
{
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "usuario_parceiro" ADD COLUMN "fk_id_parceiro" integer`
    );
    await queryRunner.query(
      `ALTER TABLE "usuario_parceiro" ADD CONSTRAINT fk_id_parceiro FOREIGN KEY (fk_id_parceiro) REFERENCES parceiro(id_parceiro)`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {}
}
