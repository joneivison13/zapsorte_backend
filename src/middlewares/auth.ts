import { Response, Request, NextFunction } from "express";
import jwt from "jsonwebtoken";
import config from "../config";

interface UserProps extends Request {
  idUsuario: Number;
}

export default (request: UserProps, response: Response, next: NextFunction) => {
  const authHeader = request.headers.authorization;

  if (!authHeader) {
    return response.status(401).send("Token não informado");
  }

  const [, token] = authHeader.split(" ");

  try {
    const payload = jwt.verify(token, config.app.secret);

    request.idUsuario = payload.id;

    return next();
  } catch (err) {
    console.log(err);
    return response.status(401).send("Token inválido");
  }
};
