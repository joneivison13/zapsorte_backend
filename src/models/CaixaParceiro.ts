import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany, ManyToOne, JoinColumn } from 'typeorm'
import Parceiro from './Parceiro';
import UsuarioParceiroCaixaParceiro from './UsuarioParceiroCaixaParceiro';

@Entity({ name: 'caixa_parceiro' })
export default class CaixaParceiro {
    @PrimaryGeneratedColumn('increment', { name: 'id_caixa_parceiro' })
    idCaixaParceiro: number;

    @Column({
      name: 'fk_id_parceiro',
      nullable: false
    })
    fkIdParceiro: string;

    @Column({
      name: 'identificador_caixa_parceiro',
      nullable: false
    })
    identificadorCaixaParceiro: string;

    @Column({
      name: 'ativo',
      nullable: false,
      default: true
    })
    ativo: boolean;

    @ManyToOne(type => Parceiro, caixaParceiro => CaixaParceiro)
    @JoinColumn({ name: 'fk_id_parceiro' })
    parceiro: Parceiro;

    @OneToMany(type => UsuarioParceiroCaixaParceiro, caixaParceiro => CaixaParceiro)
    usuarioParceiroCaixaParceiro: UsuarioParceiroCaixaParceiro[];

    @CreateDateColumn({ name: 'criado_em' })
    criadoEm: Date;

    @UpdateDateColumn({ name: 'atualizado_em' })
    atualizadoEm: Date;
}
