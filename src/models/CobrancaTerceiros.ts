import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm'
import TipoMovimentacao from './TipoMovimentacao'
import LoginUsuario from './LoginUsuario'
import MovimentacaoUsuarios from './MovimentacaoUsuarios'

@Entity({ name: 'cobranca_terceiros' })
export default class CobrancaTerceiros {
  @PrimaryGeneratedColumn('increment', { name: 'id_cobranca_terceiros' })
  idCobrancaTerceiros: number

  @Column({
    name: 'fk_id_usuario_entregador',
    nullable: false
  })
  fkIdUsuarioEntregador: number

  @Column({
    name: 'fk_movimentacao',
    nullable: false
  })
  fkMovimentacao: number

  @Column({
    name: 'cobranca_terceiros',
    nullable: false
  })
  cobrancaTerceiros: Date

  @ManyToOne(type => LoginUsuario, cobrancaTerceiros => CobrancaTerceiros)
  @JoinColumn({ name: 'fk_id_usuario_entregador' })
  loginUsuario: LoginUsuario;

  @ManyToOne(type => MovimentacaoUsuarios, cobrancaTerceiros => CobrancaTerceiros)
  @JoinColumn({ name: 'fk_movimentacao' })
  movimentacaoUsuarios: MovimentacaoUsuarios;

  @CreateDateColumn({ name: 'criado_em' })
  criadoEm: Date;

  @UpdateDateColumn({ name: 'atualizado_em' })
  atualizadoEm: Date;
}
