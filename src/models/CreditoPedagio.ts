import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm'
import LoginUsuario from './LoginUsuario'

@Entity({ name: 'credito_pedagio' })
export default class CreditoPedagio {
  @PrimaryGeneratedColumn('increment', { name: 'id_credito_pedagio' })
  idCreditoPedagio: number

  @Column({
    name: 'fk_id_login_usuario',
    nullable: false
  })
  fkIdLoginUsuario: number

  @Column({
    name: 'valor_credito_pedagio',
    precision: 11,
    scale: 2,
    nullable: false
  })
  valorCreditoPedagio: number

  @ManyToOne(type => LoginUsuario, creditoPedagio => CreditoPedagio)
  @JoinColumn({ name: 'fk_id_login_usuario' })
  loginUsuario: LoginUsuario;

  @CreateDateColumn({ name: 'criado_em' })
  criadoEm: Date;

  @UpdateDateColumn({ name: 'atualizado_em' })
  atualizadoEm: Date;
}
