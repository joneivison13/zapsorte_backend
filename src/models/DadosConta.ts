import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm'
import LoginUsuario from './LoginUsuario'

@Entity({ name: 'dados_conta' })
export default class DadosConta {
  @PrimaryGeneratedColumn('increment', { name: 'id_dados_conta' })
  idDadosConta: number

  @Column({
    name: 'fk_id_login_usuario',
    nullable: false
  })
  fkIdLoginUsuario: number

  @Column({
    nullable: false
  })
  agencia: string

  @Column({
    nullable: false
  })
  conta: string

  @Column({
    nullable: false
  })
  dvConta: string

  @ManyToOne(type => LoginUsuario, dadosConta => DadosConta)
  @JoinColumn({ name: 'fk_id_login_usuario' })
  loginUsuario: LoginUsuario;

  @CreateDateColumn({ name: 'criado_em' })
  criadoEm: Date;

  @UpdateDateColumn({ name: 'atualizado_em' })
  atualizadoEm: Date;
}
