import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm'
import PessoaEndereco from './PessoaEndereco'
import ParceiroEndereco from './ParceiroEndereco'

@Entity({ name: 'endereco' })
export default class Endereco {
    @PrimaryGeneratedColumn('increment', { name: 'id_endereco' })
    idEndereco: number;

    @Column({
      length: 8,
      nullable: false,
      name: 'cep'
    })
    cep: string;

    @Column({
      nullable: false
    })
    logradouro: string;

    @Column({
      nullable: false
    })
    bairro: string;

    @Column({
      nullable: false
    })
    localidade: string;

    @Column({
      nullable: false,
      length: 2
    })
    uf: string;

    @OneToMany(type => PessoaEndereco, endereco => Endereco)
    pessoaEnderecos: PessoaEndereco[];

    @OneToMany(type => ParceiroEndereco, endereco => Endereco)
    ParceiroEndereco: ParceiroEndereco[];
}
