import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm'
import TipoMovimentacao from './TipoMovimentacao'
import LoginUsuario from './LoginUsuario'
import CobrancaTerceiros from './CobrancaTerceiros'
import Banco from './Banco'

@Entity({ name: 'movimentacao_usuarios' })
export default class MovimentacaoUsuarios {
  @PrimaryGeneratedColumn('increment', { name: 'id_movimentacao_usuarios' })
  idMovimentacaoUsuarios: number

  @Column({
    name: 'fk_id_tipo_movimentacao',
    nullable: false
  })
  fkIdTipoMovimentacao: number

  @Column({
    name: 'fk_pagador',
    nullable: true
  })
  fkPagador: number

  @Column({
    name: 'fk_recebedor',
    nullable: true
  })
  fkRecebedor: number

  @Column({
    name: 'descricao_movimentacao',
    nullable: true
  })
  descricaoMovimentacao: string

  @Column({
    name: 'valor_movimentacao',
    nullable: false,
    precision: 11,
    scale: 2
  })
  valorMovimentacao: number

  @Column({
    name: 'data_movimentacao',
    nullable: false
  })
  dataMovimentacao: Date

  @Column({
    nullable: true
  })
  agencia: string

  @Column({
    name: 'conta_corrente',
    nullable: true
  })
  contaCorrente: string

  @Column({
    name: 'cpf_transferencia',
    nullable: true
  })
  cpfTransferencia: string

  @Column({
    name: 'nome_transferencia',
    nullable: true
  })
  nomeTransferencia: string

  @Column({
    name: 'fk_banco',
    nullable: true
  })
  fkBanco: number

  @ManyToOne(type => Banco, movimentacaoUsuarios => MovimentacaoUsuarios)
  @JoinColumn({ name: 'fk_banco' })
  banco: Banco;

  @ManyToOne(type => TipoMovimentacao, movimentacaoUsuarios => MovimentacaoUsuarios)
  @JoinColumn({ name: 'fk_id_tipo_movimentacao' })
  tipoMovimentacao: TipoMovimentacao;

  @ManyToOne(type => LoginUsuario, movimentacaoUsuarios => MovimentacaoUsuarios)
  @JoinColumn({ name: 'fk_pagador' })
  pagador: LoginUsuario;

  @ManyToOne(type => LoginUsuario, movimentacaoUsuarios => MovimentacaoUsuarios)
  @JoinColumn({ name: 'fk_recebedor' })
  recebedor: LoginUsuario;

  @OneToMany(type => CobrancaTerceiros, movimentacaoUsuarios => MovimentacaoUsuarios)
  cobrancaTerceiros: CobrancaTerceiros[];
}
