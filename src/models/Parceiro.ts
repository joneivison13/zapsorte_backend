import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  OneToMany,
  JoinColumn,
} from "typeorm";
import ParceiroEndereco from "./ParceiroEndereco";
import CaixaParceiro from "./CaixaParceiro";
// import UsuarioParceiroParceiro from "./UsuarioParceiroParceiro";
import MovimentacaoParceiros from "./MovimentacaoParceiros";
import Titulo from "./Titulo";

@Entity({ name: "parceiro" })
export default class Parceiro {
  @PrimaryGeneratedColumn("increment", { name: "id_parceiro" })
  idParceiro: number;

  @Column({
    name: "razao_social",
    nullable: false,
  })
  razaoSocial: string;

  @Column({
    name: "nome_fantasia",
    nullable: true,
  })
  nomeFantasia: string;

  @Column({
    nullable: true,
    length: 14,
  })
  cnpj: string;

  @Column({
    nullable: true,
    default: true,
  })
  ativado: boolean;

  @Column({
    name: "telefone_contato_1",
    nullable: false,
    length: 11,
  })
  telefoneContato1: string;

  @Column({
    name: "telefone_contato_2",
    nullable: true,
    length: 11,
  })
  telefoneContato2: string;

  @Column({
    name: "telefone_contato_3",
    nullable: true,
    length: 11,
  })
  telefoneContato3: string;

  @Column({
    name: "telefone_contato_4",
    nullable: true,
    length: 11,
  })
  telefoneContato4: string;

  @Column({
    name: "email_1",
    nullable: false,
  })
  email1: string;

  @Column({
    name: "email_2",
    nullable: true,
  })
  email2: string;

  @Column({
    name: "email_3",
    nullable: true,
  })
  email3: string;

  @Column({
    name: "email_4",
    nullable: true,
  })
  email4: string;

  @Column({
    name: "pessoa_contato",
    nullable: true,
  })
  pessoaContato: string;

  @Column({
    name: "celular_contato",
    nullable: true,
    length: 11,
  })
  celularContato: string;

  @Column({
    name: "email_contato",
    nullable: true,
  })
  emailContato: string;

  @OneToMany((type) => Titulo, (parceiro) => Parceiro)
  titulos: Titulo[];

  @OneToMany((type) => ParceiroEndereco, (parceiro) => Parceiro)
  parceiroEndereco: ParceiroEndereco[];

  @OneToMany((type) => CaixaParceiro, (parceiro) => Parceiro)
  caixasParceiro: CaixaParceiro[];

  // @OneToMany(type => UsuarioParceiroParceiro, parceiro => Parceiro)
  // usuariosParceiroParceiro: UsuarioParceiroParceiro[];

  @OneToMany((type) => MovimentacaoParceiros, (parceiro) => Parceiro)
  movimentacoesParceiro: MovimentacaoParceiros[];

  @CreateDateColumn({ name: "criado_em" })
  criadoEm: Date;

  @UpdateDateColumn({ name: "atualizado_em" })
  atualizadoEm: Date;
}
