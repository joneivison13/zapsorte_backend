import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import UsuarioParceiroNivelAcesso from "./UsuarioParceiroNivelAcesso";
import Pessoa from "./Pessoa";
import Endereco from "./Endereco";
import PessoaEnderecos from "./PessoaEndereco";

@Entity({ name: "pessoa_endereco" })
export default class PessoaEndereco {
  // @PrimaryGeneratedColumn('increment', { name: 'id_pessoa_endereco' })
  // idPessoaEndereco: number;

  @Column({
    name: "fk_id_pessoa",
    nullable: false,
    primary: true,
  })
  fkIdPessoa: number;

  @Column({
    name: "fk_id_endereco",
    nullable: false,
    primary: true,
  })
  fkIdEndereco: number;

  @Column({
    nullable: false,
  })
  complemento: string;

  @Column({
    nullable: true,
  })
  latitude: string;

  @Column({
    nullable: true,
  })
  longitude: string;

  @ManyToOne((type) => Pessoa, (pessoaEnderecos) => PessoaEnderecos)
  @JoinColumn({ name: "fk_id_pessoa" })
  pessoas: Pessoa;

  @ManyToOne((type) => Endereco, (pessoaEnderecos) => PessoaEnderecos)
  @JoinColumn({ name: "fk_id_endereco" })
  enderecos: Endereco;

  @CreateDateColumn({ name: "criado_em" })
  criadoEm: Date;

  @UpdateDateColumn({ name: "atualizado_em" })
  atualizadoEm: Date;
}
