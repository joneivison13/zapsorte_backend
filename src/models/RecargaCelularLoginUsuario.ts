import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, OneToMany, ManyToOne, JoinColumn } from 'typeorm'
import LoginUsuario from './LoginUsuario' 
import RecargaCelular from './RecargaCelular'

@Entity({ name: 'recarga_celular_login_usuario' })
export default class RecargaCelularLoginUsuario {
    @PrimaryGeneratedColumn('increment', { name: 'id_recarga_celular_login_usuario' })
    idRecargaCelularLoginUsuario: number;

    @Column({
      name: 'fk_id_login_usuario',
      nullable: false,
    })
    fkIdLoginUsuario: number;

    @Column({
      name: 'fk_id_recarga_celular',
      nullable: false,
    })
    fkIdRecargaCelular: number;

    @Column({
      nullable: false,
    })
    complemento: string;

    @Column({
      nullable: true,
    })
    latitude: string;

    @Column({
      nullable: true,
    })
    longitude: string;

    @ManyToOne(type => LoginUsuario, recargasCelularLoginUsuario => RecargaCelularLoginUsuario)
    @JoinColumn({ name: 'fk_id_login_usuario' })
    loginUsuario: LoginUsuario;

    @ManyToOne(type => RecargaCelular, recargasCelularLoginUsuario => RecargaCelularLoginUsuario)
    @JoinColumn({ name: 'fk_id_recarga_celular' })
    recargaCelular: RecargaCelular;

    @CreateDateColumn({ name: 'criado_em' })
    criadoEm: Date;

    @UpdateDateColumn({ name: 'atualizado_em' })
    atualizadoEm: Date;
}