import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany, JoinColumn } from 'typeorm'
import Parceiro from './Parceiro'
import RegistroTitulo from './RegistroTitulo'

@Entity({ name: 'titulo' })
export default class Titulo {
  @PrimaryGeneratedColumn('increment', { name: 'id_titulo' })
  idTitulo: number

  @Column({
    name: 'fk_id_parceiro',
    nullable: false
  })
  fkIdParceiro: number

  @Column({
    name: 'numero_titulo',
    nullable: false
  })
  numeroTitulo: string

  @Column({
    name: 'data_lancamento',
    nullable: false
  })
  dataLancamento: Date

  @Column({
    name: 'data_vencimento',
    nullable: false
  })
  dataVencimento: Date

  @ManyToOne(type => Parceiro, titulos => Titulo)
  @JoinColumn({ name: 'fk_id_parceiro' })
  parceiro: Parceiro;

  @OneToMany(type => RegistroTitulo, titulo => Titulo)
  registrosTitulo: RegistroTitulo[];
}
