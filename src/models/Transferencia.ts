import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn, OneToMany } from 'typeorm'
import Banco from './Banco'
import LoginUsuario from './LoginUsuario'
import MovimentacaoUsuarios from './MovimentacaoUsuarios'

@Entity({ name: 'transferencia' })
export default class Transferencia {
  @PrimaryGeneratedColumn('increment', { name: 'id_transferencia' })
  idTransferencia: number

  @Column({
    name: 'fk_id_banco',
    nullable: false
  })
  fkIdBanco: number

  @Column({
    name: 'fk_id_login_usuario',
    nullable: false
  })
  fkIdLoginUsuario: number

  @Column({
    name: 'fk_id_movimentacao_usuarios',
    nullable: false
  })
  fkIdMovimentacaoUsuarios: number

  @Column({
    name: 'conta_corrente',
    nullable: false
  })
  contaCorrente: string

  @Column({
    name: 'agencia',
    nullable: false
  })
  agencia: number

  @Column({
    name: 'cpf_destino',
    nullable: false
  })
  cpfDestino: string

  @Column({
    name: 'nome_completo_destino',
    nullable: false
  })
  nomeCompletoDestino: string

  @ManyToOne(type => Banco, transferencia => Transferencia)
  @JoinColumn({ name: 'fk_id_banco' })
  banco: Banco;

  @ManyToOne(type => LoginUsuario, transferencia => Transferencia)
  @JoinColumn({ name: 'fk_id_login_usuario' })
  loginUsuario: LoginUsuario;

  @ManyToOne(type => MovimentacaoUsuarios, transferencia => Transferencia)
  @JoinColumn({ name: 'fk_id_movimentacao_usuarios' })
  movimentacaoUsuarios: MovimentacaoUsuarios;
}
