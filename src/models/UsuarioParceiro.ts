import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
} from "typeorm";
import UsuarioParceiroNivelAcesso from "./UsuarioParceiroNivelAcesso";
// import UsuarioParceiroParceiro from "./UsuarioParceiroParceiro";

@Entity({ name: "usuario_parceiro" })
export default class UsuarioParceiro {
  @PrimaryGeneratedColumn("increment", { name: "id_usuario_parceiro" })
  idUsuarioParceiro: number;

  @Column({
    length: 11,
    unique: true,
  })
  cpf: string;

  @Column({
    nullable: false,
  })
  password: string;

  @Column({
    default: true,
    nullable: false,
  })
  ativo: boolean;

  @OneToMany(
    (type) => UsuarioParceiroNivelAcesso,
    (usuarioParceiro) => UsuarioParceiro
  )
  usuarioParceiroNiveisAcesso: UsuarioParceiroNivelAcesso[];

  // @OneToMany(type => UsuarioParceiroParceiro, usuarioParceiro => UsuarioParceiro)
  // usuariosParceiroParceiro: UsuarioParceiroParceiro[];

  @CreateDateColumn({ name: "criado_em" })
  criadoEm: Date;

  @UpdateDateColumn({ name: "atualizado_em" })
  atualizadoEm: Date;
}
