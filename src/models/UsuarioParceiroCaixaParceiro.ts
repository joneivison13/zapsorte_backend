import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm'
import CaixaParceiro from './CaixaParceiro'
import NivelAcesso from './NivelAcesso'

@Entity({ name: 'usuario_parceiro_caixa_parceiro' })
export default class UsuarioParceiroCaixaParceiro {
  @Column({
    name: 'fk_id_caixa_parceiro',
    nullable: false,
    primary: true
  })
  fkIdCaixaParceiro: number

  @Column({
    name: 'fk_id_nivel_acesso',
    nullable: false,
    primary: true
  })
  fkIdNivelAcesso: number

  @ManyToOne(type => CaixaParceiro, usuarioParceiroCaixaParceiro => UsuarioParceiroCaixaParceiro)
  @JoinColumn({ name: 'fk_id_usuario_parceiro' })
  caixaParceiro: CaixaParceiro;

  @ManyToOne(type => NivelAcesso, usuarioParceiroCaixaParceiro => UsuarioParceiroCaixaParceiro)
  @JoinColumn({ name: 'fk_id_nivel_acesso' })
  nivelAcesso: NivelAcesso;
}
