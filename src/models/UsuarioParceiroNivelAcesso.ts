import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm'
import UsuarioParceiro from './UsuarioParceiro'
import NivelAcesso from './NivelAcesso'

@Entity({ name: 'usuario_parceiro_nivel_acesso' })
export default class UsuarioParceiroNivelAcesso {
  @Column({
    name: 'fk_id_usuario_parceiro',
    nullable: false,
    primary: true
  })
  fkIdUsuarioParceiro: number

  @Column({
    name: 'fk_id_nivel_acesso',
    nullable: false,
    primary: true
  })
  fkIdNivelAcesso: number

  @ManyToOne(type => UsuarioParceiro, usuarioParceiroNiveisAcesso => UsuarioParceiroNivelAcesso)
  @JoinColumn({ name: 'fk_id_usuario_parceiro' })
  usuarioParceiro: UsuarioParceiro;

  @ManyToOne(type => NivelAcesso, usuarioParceiroNiveisAcesso => UsuarioParceiroNivelAcesso)
  @JoinColumn({ name: 'fk_id_nivel_acesso' })
  nivelAcesso: NivelAcesso;
}
