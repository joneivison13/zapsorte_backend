import { Repository, EntityRepository } from 'typeorm'
import Endereco from '../models/Endereco'

@EntityRepository(Endereco)
export default class EnderecoRepository extends Repository<Endereco> {
}
