import { Repository, EntityRepository } from 'typeorm'
import Indicacao from '../models/Indicacao'

@EntityRepository(Indicacao)
export default class IndicacaoRepository extends Repository<Indicacao> {
}
