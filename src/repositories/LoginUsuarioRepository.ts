import { Repository, EntityRepository } from 'typeorm'
import LoginUsuario from '../models/LoginUsuario'

@EntityRepository(LoginUsuario)
export default class LoginUsuarioRepository extends Repository<LoginUsuario> {}
