import { Repository, EntityRepository } from 'typeorm'
import MovimentacaoParceiros from '../models/MovimentacaoParceiros'

@EntityRepository(MovimentacaoParceiros)
export default class MovimentacaoParceirosRepository extends Repository<MovimentacaoParceiros> {
}
