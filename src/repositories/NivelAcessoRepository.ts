import { Repository, EntityRepository } from 'typeorm'
import NivelAcesso from '../models/NivelAcesso'

@EntityRepository(NivelAcesso)
export default class NivelAcessoRepository extends Repository<NivelAcesso> {}
