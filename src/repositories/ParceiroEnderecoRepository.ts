import { Repository, EntityRepository } from 'typeorm'
import ParceiroEndereco from '../models/ParceiroEndereco'

@EntityRepository(ParceiroEndereco)
export default class ParceiroEnderecoRepository extends Repository<ParceiroEndereco> {
}
