import { Repository, EntityRepository } from 'typeorm'
import Parceiro from '../models/Parceiro'

@EntityRepository(Parceiro)
export default class ParceiroRepository extends Repository<Parceiro> {
}
