import { Repository, EntityRepository } from 'typeorm'
import PessoaEndereco from '../models/PessoaEndereco'

@EntityRepository(PessoaEndereco)
export default class PessoaEnderecoRepository extends Repository<PessoaEndereco> {
}
