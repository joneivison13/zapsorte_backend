import { Repository, EntityRepository } from 'typeorm'
import RecargaCelularLoginUsuario from '../models/RecargaCelularLoginUsuario'

@EntityRepository(RecargaCelularLoginUsuario)
export default class RecargaCelularLoginUsuarioRepository extends Repository<RecargaCelularLoginUsuario> {
}
