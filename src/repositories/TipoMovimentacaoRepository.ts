import { Repository, EntityRepository } from 'typeorm'
import TipoMovimentacao from '../models/TipoMovimentacao'

@EntityRepository(TipoMovimentacao)
export default class TipoMovimentacaoRepository extends Repository<TipoMovimentacao> {
}
