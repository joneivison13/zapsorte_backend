import { Repository, EntityRepository } from 'typeorm'
import Transferencia from '../models/Transferencia'

@EntityRepository(Transferencia)
export default class TransferenciaRepository extends Repository<Transferencia> {
}
