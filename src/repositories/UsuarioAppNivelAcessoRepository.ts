import { Repository, EntityRepository } from 'typeorm'
import UsuarioAppNivelAcesso from '../models/UsuarioAppNivelAcesso'

@EntityRepository(UsuarioAppNivelAcesso)
export default class UsuarioAppNivelAcessoRepository extends Repository<UsuarioAppNivelAcesso> {}
