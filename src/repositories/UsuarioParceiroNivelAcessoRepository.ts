import { Repository, EntityRepository } from 'typeorm'
import UsuarioParceiroNivelAcesso from '../models/UsuarioParceiroNivelAcesso'

@EntityRepository(UsuarioParceiroNivelAcesso)
export default class UsuarioParceiroNivelAcessoRepository extends Repository<UsuarioParceiroNivelAcesso> {}
