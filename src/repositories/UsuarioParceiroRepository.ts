import { Repository, EntityRepository } from 'typeorm'
import UsuarioParceiro from '../models/UsuarioParceiro'

@EntityRepository(UsuarioParceiro)
export default class UsuarioParceiroRepository extends Repository<UsuarioParceiro> {}
