import { Repository, EntityRepository } from 'typeorm'
import UsuarioPin from '../models/UsuarioPin'

@EntityRepository(UsuarioPin)
export default class UsuarioPinRepository extends Repository<UsuarioPin> {}
