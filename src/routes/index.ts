/* eslint-disable semi */
/* eslint-disable no-unused-vars */
/* eslint-disable quotes */
import { Request, Response, Router } from "express";
import LoginUsuarioController from "../controllers/LoginUsuarioController";
import UsuarioParceiroController from "../controllers/UsuarioParceiroController";
import EnderecoController from "../controllers/EnderecoController";
import SessionController from "../controllers/SessionController";
import authMiddleware from "../middlewares/auth";
import NivelAcessoController from "../controllers/NivelAcessoController";
import TituloController from "../controllers/TituloController";
import RegistroTituloController from "../controllers/RegistroTituloController";
import UsuarioPinController from "../controllers/UsuarioPinController";
import MovimentacaoUsuariosController from "../controllers/MovimentacaoUsuariosController";
import BancoController from "../controllers/BancoController";
import TransferenciaController from "../controllers/TransferenciaController";

import multer from "multer";
import multerConfig from "../config/multer";
const routes = Router();

routes.get("/", (request: Request, response: Response) => {
  response.send("Server running!!!");
});

routes.get("/endereco/:cep", EnderecoController.getByCep);
routes.get("/endereco-usuario/:idUsuario", EnderecoController.get);
routes.post("/endereco", EnderecoController.store);
routes.post("/usuario", LoginUsuarioController.store);
routes.get(
  "/usuario-celular/:celularUsuario",
  LoginUsuarioController.getByCelular
);
routes.get("/usuario-cpf/:cpfUsuario", LoginUsuarioController.getByCPF);
routes.post("/usuario-parceiro", UsuarioParceiroController.store);
routes.get("/usuario-parceiro-cnpj/:cnpj", UsuarioParceiroController.getByCNPJ);
routes.post("/sessions-app", SessionController.store);
routes.post("/sessions-parceiro", SessionController.storeParceiro);
routes.post("/pin", UsuarioPinController.generate);
routes.post("/validate-pin", UsuarioPinController.validate);
routes.post(
  "/pin-recuperar-senha",
  UsuarioPinController.generateRecuperarSenha
);
routes.post(
  "/session-recuperar-senha",
  SessionController.sessionRecuperarSenha
);

routes.use(authMiddleware);

routes.put("/edit-password-app", SessionController.updateLoginUsuario);
routes.put("/edit-password-parceiro", SessionController.updateUsuarioParceiro);
routes.post("/nivel-acesso", NivelAcessoController.store);
routes.get("/nivel-acesso", NivelAcessoController.get);
routes.put("/nivel-acesso", NivelAcessoController.update);
routes.delete("/nivel-acesso/:idNivelAcesso", NivelAcessoController.delete);
routes.post("/titulo", TituloController.store);
routes.get("/titulo", TituloController.get);
routes.put("/titulo", TituloController.update);
routes.delete("/titulo/:idTitulo", TituloController.delete);
routes.get("/usuario-parceiros", UsuarioParceiroController.getParceiros);
routes.get(
  "/titulos-usuario/:fkIdLoginUsuario",
  RegistroTituloController.getByUsuario
);
routes.get("/ganhadores", RegistroTituloController.getGanhadores);
routes.post("/registrar-titulo", RegistroTituloController.store);
routes.put("/usuario", LoginUsuarioController.update);
routes.post(
  "/usuario-upload",
  multer(multerConfig).single("file"),
  LoginUsuarioController.upload
);
routes.post("/gerar-troco", MovimentacaoUsuariosController.store);
routes.post("/banco", BancoController.store);
routes.put("/banco", BancoController.update);
routes.delete("/banco/:idBanco", BancoController.delete);
routes.get("/banco", BancoController.get);
routes.post("/adicionar-saldo-pedagio", MovimentacaoUsuariosController.store);
routes.post("/pagar-com-qr-code", MovimentacaoUsuariosController.store);
routes.post("/pagar-sem-celular", MovimentacaoUsuariosController.store);
routes.post("/pagar-informe-recebedor", MovimentacaoUsuariosController.store);
routes.post("/historico", MovimentacaoUsuariosController.getHistorico);
routes.post(
  "/transferencia",
  MovimentacaoUsuariosController.storeTransferencia
);

export default routes;
