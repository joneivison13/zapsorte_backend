import { Request, Response } from 'express'
import BancoRepository from '../repositories/BancoRepository'
import TransferenciaRepository from '../repositories/TransferenciaRepository'
import { getCustomRepository } from 'typeorm'

class BancoService {
  async store (request: Request, response: Response) {
    try {
      const { nomeBanco, codigoFebrace } = request.body

      const repo = getCustomRepository(BancoRepository)

      const banco = await repo.save({
        nomeBanco,
        codigoFebrace
      })

      response.json(banco)
    } catch (err) {
      console.log(err.message)
    }
  }

  async update (request: Request, response: Response) {
    try {
      const { idBanco, nomeBanco, codigoFebrace } = request.body

      const repo = getCustomRepository(BancoRepository)

      const { affected } = await repo.update(idBanco, {
        nomeBanco,
        codigoFebrace
      })

      response.json({ affected })
    } catch (err) {
      console.log(err.message)
    }
  }

  async get (request: Request, response: Response) {
    try {
      const repo = getCustomRepository(BancoRepository)
      const resp = await repo.find()

      response.json(resp)
    } catch (err) {
      console.log(err.message)
    }
  }

  async delete (request: Request, response: Response) {
    try {
      const { idBanco } = request.params
      const repo = getCustomRepository(BancoRepository)
      const repoT = getCustomRepository(TransferenciaRepository)

      const existApp = await repoT.findOne({ fkIdBanco: Number(idBanco) })
      if (existApp) { response.status(401).send('Existe transferência associada a este banco!') }

      const resp = await repo.delete(idBanco)

      response.json(resp)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new BancoService()
