import { Request, Response } from "express";
import LoginUsuarioRepository from "../repositories/LoginUsuarioRepository";
import PessoaRepository from "../repositories/PessoaRepository";
import PessoaService from "../services/PessoaService";
import EnderecoService from "../services/EnderecoService";
import NivelAcessoService from "../services/NivelAcessoService";
import UsuarioAppNivelAcessoService from "../services/UsuarioAppNivelAcessoService";
import bcrypt from "bcrypt";
import { getCustomRepository } from "typeorm";
import config from "../config";
import EnderecoRepository from "../repositories/EnderecoRepository";
import PessoaEnderecoRepository from "../repositories/PessoaEnderecoRepository";

class LoginUsuarioService {
  async store(request: Request, response: Response) {
    try {
      const {
        cpfUsuario,
        celularUsuario,
        password,
        nome,
        ultimoNome,
        rgUsuario,
        nascimento,
        genero,
        email,
        idEndereco,
        cep,
        logradouro,
        complemento,
        bairro,
        localidade,
        uf,
      } = request.body;

      const repoLoginUsuario = getCustomRepository(LoginUsuarioRepository);

      const exist = await repoLoginUsuario.findOne({
        where: [{ cpf: cpfUsuario }, { numeroCelular: celularUsuario }],
      });

      if (exist) {
        return response.status(401).send("Usuário já existe");
      }

      const nivelAcesso = await NivelAcessoService.getDefaultApp();

      if (!nivelAcesso) {
        response.status(500).send('Nível de acesso "APP" não encontrado');
      }

      const passwordHash = await bcrypt.hash(password, Number(config.app.salt));

      const loginUsuario = await repoLoginUsuario.save({
        cpf: cpfUsuario,
        email,
        numeroCelular: celularUsuario,
        passwordUsuario: passwordHash,
        ativado: true,
      });

      delete loginUsuario.passwordUsuario;

      const pessoa = await PessoaService.store(
        loginUsuario.idLoginUsuario,
        nome,
        ultimoNome,
        nascimento,
        "",
        genero
      );

      await UsuarioAppNivelAcessoService.store(
        nivelAcesso.idNivelAcesso,
        loginUsuario.idLoginUsuario
      );

      response.json({ loginUsuario, pessoa });
    } catch (err) {
      console.log(err.message);
    }
  }

  async update(request: Request, response: Response) {
    try {
      const {
        idUsuario,
        cpfUsuario,
        nome,
        ultimoNome,
        celularUsuario,
        email,
        rgUsuario,
        nascimento,
        genero,
        idCliente,
        cep,
        logradouro,
        complemento,
        bairro,
        localidade,
        uf,
        latitude,
        longitude,
        loginUsuarioId,
      } = request.body;
      console.log(request.body);

      console.log(1);

      const repoLoginUsuario = getCustomRepository(LoginUsuarioRepository);
      const repoEndereco = getCustomRepository(EnderecoRepository);
      const repoPessoaEndereco = getCustomRepository(PessoaEnderecoRepository);

      const existUsu = await repoLoginUsuario.findOne({
        where: [{ cpf: cpfUsuario }],
      });

      console.log(cpfUsuario, existUsu, +loginUsuarioId);

      if (!existUsu) {
        return response.status(401).send("Este CPF não existe na base.");
      }
      console.log(2);

      if (existUsu && +existUsu.idLoginUsuario !== +loginUsuarioId) {
        return response.status(401).send("Este CPF pertence a outra conta.");
      }
      console.log(3);

      let loginUsuario: any = await repoLoginUsuario.update(
        { idLoginUsuario: loginUsuarioId },
        {
          cpf: cpfUsuario,
        }
      );
      console.log(4);

      // const pessoa = await PessoaService.update(
      //   idCliente,
      //   loginUsuario.idLoginUsuario,
      //   nome,
      //   ultimoNome,
      //   nascimento,
      //   "",
      //   genero
      // );

      let existEndereco: any = await repoEndereco.findOne({
        where: [{ cep }],
      });

      console.log(5);

      if (!existEndereco) {
        console.log(5.1);
        existEndereco = await repoEndereco.save({
          cep,
          logradouro,
          bairro,
          localidade,
          uf,
        });
      }
      console.log(6, existEndereco);

      let endereco: any = await repoPessoaEndereco.findOne({
        where: [{ fkIdPessoa: loginUsuarioId }],
      });
      let enderecoDb: any = false;
      console.log(7);

      if (!endereco) {
        console.log("insert", {
          fkIdEndereco: existEndereco.idEndereco,
          fkIdPessoa: loginUsuarioId,
          complemento,
          latitude,
          longitude,
        });
        endereco = await repoPessoaEndereco.save({
          fkIdEndereco: existEndereco.idEndereco,
          fkIdPessoa: loginUsuarioId,
          complemento,
          latitude,
          longitude,
        });
      } else {
        console.log("endereco id", existEndereco.idEndereco);
        endereco = await repoPessoaEndereco.update(
          {
            fkIdPessoa: loginUsuarioId,
          },
          {
            fkIdEndereco: existEndereco.idEndereco,
            fkIdPessoa: loginUsuarioId,
            complemento,
            latitude,
            longitude,
          }
        );
      }

      console.log("end", endereco);

      loginUsuario.idUsuario = idUsuario;
      loginUsuario.cpfUsuario = cpfUsuario;
      loginUsuario.avatar = existUsu.avatar;
      loginUsuario.email = existUsu.email;
      loginUsuario.ativado = existUsu.ativado;
      loginUsuario.celularUsuario = celularUsuario;
      console.log(8);
      delete loginUsuario.generatedMaps;
      delete loginUsuario.raw;
      delete loginUsuario.affected;

      console.log(9);

      response.json({ usuario: loginUsuario, endereco, existEndereco });
    } catch (err) {
      console.log(err.message, err);
    }
  }

  async getByCelular(request: Request, response: Response) {
    try {
      const { celularUsuario } = request.params;
      const repoU = getCustomRepository(LoginUsuarioRepository);
      const repoC = getCustomRepository(PessoaRepository);
      const usuario: any = await repoU.findOne({
        numeroCelular: celularUsuario,
      });

      if (usuario) {
        delete usuario.passwordUsuario;
        const pessoa = await repoC.findOne({
          fkIdLoginUsuario: usuario.idLoginUsuario,
        });
        usuario.pessoa = pessoa;
      }

      response.json(usuario);
    } catch (err) {
      console.log(err.message);
    }
  }

  async getByCPF(request: Request, response: Response) {
    try {
      const { cpfUsuario } = request.params;
      const repoU = getCustomRepository(LoginUsuarioRepository);
      const repoC = getCustomRepository(PessoaRepository);
      const usuario: any = await repoU.findOne({ cpf: cpfUsuario });

      if (usuario) {
        delete usuario.passwordUsuario;
        const pessoa = await repoC.findOne({
          fkIdLoginUsuario: usuario.idLoginUsuario,
        });
        usuario.pessoa = pessoa;
      }

      response.json(usuario);
    } catch (err) {
      console.log(err.message);
    }
  }

  async upload(request: Request, response: Response) {
    try {
      const { filename } = request.file;
      const { idUsuario } = request.body;
      const repo = getCustomRepository(LoginUsuarioRepository);

      await repo.update({ idLoginUsuario: idUsuario }, { avatar: filename });

      response.json({ filename });
    } catch (err) {
      console.log(err.message);
    }
  }
}

export default new LoginUsuarioService();
