import nodemailer from 'nodemailer'
import settings from '../config'

class Mail {
    async send(subject, to, html) {
        const transporter = nodemailer.createTransport({
            host: settings.mail.smtp,
            port: Number(settings.mail.smtp_port),
            auth: {
                user: settings.mail.user_email,
                pass: settings.mail.user_pass
            }
        })

        transporter.sendMail({
            from: settings.mail.user_email,
            to,
            subject,
            html
        }).then(info => {

            return info
        }).catch(error => {
        
            console.log(error)
            return error
        })
    }
}

export default new Mail()
