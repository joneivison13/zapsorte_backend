import MovimentacaoUsuariosRepository from '../repositories/MovimentacaoUsuariosRepository'
import PessoaRepository from '../repositories/PessoaRepository'
import { getCustomRepository } from 'typeorm'
import { Request, Response } from 'express'

class MovimentacaoUsuariosService {
  async store (request: Request, response: Response) {
    try {
      const { fkIdTipoMovimentacao, fkPagador, fkRecebedor, descricaoMovimentacao, valorMovimentacao, dataMovimentacao } = request.body

      const repo = getCustomRepository(MovimentacaoUsuariosRepository)

      const registro = await repo.save({
        fkIdTipoMovimentacao,
        fkPagador, 
        fkRecebedor, 
        descricaoMovimentacao, 
        valorMovimentacao, 
        dataMovimentacao
      })

      return response.json(registro)
    } catch (err) {
      console.log(err.message)
    }
  }

  async storeTransferencia (request: Request, response: Response) {
    try {
      const { fkIdTipoMovimentacao, fkPagador, fkRecebedor, descricaoMovimentacao, valorMovimentacao, dataMovimentacao, fkBanco, agencia, contaCorrente, cpfTransferencia, nomeTransferencia } = request.body

      const repo = getCustomRepository(MovimentacaoUsuariosRepository)

      const registro = await repo.save({
        fkIdTipoMovimentacao,
        fkPagador, 
        fkRecebedor, 
        descricaoMovimentacao, 
        valorMovimentacao, 
        dataMovimentacao,
        fkBanco,
        agencia, 
        contaCorrente, 
        cpfTransferencia, 
        nomeTransferencia
      })

      return response.json(registro)
    } catch (err) {
      console.log(err.message)
    }
  }

  async getHistorico (request: Request, response: Response) {
    try {
      const { dataIni, dataFim, fkIdLoginUsuario, cpf } = request.body

      const repo = getCustomRepository(MovimentacaoUsuariosRepository)
      const repoP = getCustomRepository(PessoaRepository)

      let registro = await repo.createQueryBuilder('movimentacao_usuario')
      .innerJoinAndSelect('movimentacao_usuario.tipoMovimentacao', 'tipo_movimentacao')
      .innerJoinAndSelect('movimentacao_usuario.pagador', 'login_usuario')
      .leftJoinAndSelect('movimentacao_usuario.recebedor', 'login_usuario as recebedor')
      .leftJoinAndSelect('movimentacao_usuario.banco', 'banco')
      .where(`data_movimentacao BETWEEN '${dataIni}' AND '${dataFim}'`)
      .andWhere(`(fk_pagador = ${fkIdLoginUsuario} OR fk_recebedor = ${fkIdLoginUsuario} OR cpf_transferencia = '${cpf}')`)
      .orderBy(`movimentacao_usuario.data_movimentacao`, 'DESC')
      .getMany()  

      let arrParceNew = []
      let arrParse = JSON.parse(JSON.stringify(registro))

      if ( arrParse.length > 0 ) {
        arrParse.map(async (item: any, i) => {

          let objPessoa = await repoP.findOne({where: {fkIdLoginUsuario: item.fkPagador}})
          item.nomePessoa = objPessoa.nomePessoa + ' ' + objPessoa.ultimoNomePessoa
          
          item.nomePessoaRecebedor = ''
          if ( item.fkRecebedor ) {
            let objPessoaRecebedor = await repoP.findOne({where: {fkIdLoginUsuario: item.fkRecebedor}})
            item.nomePessoaRecebedor = objPessoaRecebedor.nomePessoa + ' ' + objPessoaRecebedor.ultimoNomePessoa
          }

          arrParceNew.push(item)
        
          if (arrParse.length === arrParceNew.length) return response.json(arrParceNew)

        })
      } else {
        return response.json(arrParceNew)
      }

    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new MovimentacaoUsuariosService()
