import ParceiroRepository from '../repositories/ParceiroRepository'
import { getCustomRepository } from 'typeorm'
import { Request, Response } from 'express'
import ParceiroEnderecoRepository from '../repositories/ParceiroEnderecoRepository'
import EnderecoRepository from '../repositories/EnderecoRepository'

class ParceiroService {
  async store (
    fkIdUsuarioParceiro: number,
    fkIdEndereco: number,
    razaoSocial: string,
    nomeFantasia: string,
    telefoneContato1: string,
    telefoneContato2: string,
    telefoneContato3: string,
    telefoneContato4: string,
    email1: string,
    email2: string,
    email3: string,
    email4: string,
    pessoaContato: string,
    celularContato: string,
    emailContato: string,
    latitude: string,
    longitude: string
  ) {
    try {
      const repo = getCustomRepository(ParceiroRepository)

      return await repo.save({
        fkIdUsuarioParceiro,
        fkIdEndereco,
        razaoSocial,
        nomeFantasia,
        telefoneContato1,
        telefoneContato2,
        telefoneContato3,
        telefoneContato4,
        email1,
        email2,
        email3,
        email4,
        pessoaContato,
        celularContato,
        emailContato,
        latitude,
        longitude
      })
    } catch (err) {
      console.log(err.message)
    }
  }

  async get (request: Request, response: Response) {
    try {

      const repoPE = getCustomRepository(ParceiroEnderecoRepository)
      const repoE = getCustomRepository(EnderecoRepository)
      const repoP = getCustomRepository(ParceiroRepository)
      const resp = await repoP.find()

      var newRegistro = []
      if (resp.length > 0) {
        for (var i = 0; i < resp.length; i = i + 1) {
          let endeJoin = await repoPE.findOne({ fkIdParceiro: resp[i].idParceiro })

          newRegistro[i] = resp[i]
          let endeCompl = null
          if ( endeJoin ) {
            endeCompl = await repoE.findOne({ idEndereco: endeJoin.fkIdEndereco })
            newRegistro[i].cep = endeCompl.cep
            newRegistro[i].logradouro = endeCompl.logradouro
            newRegistro[i].bairro = endeCompl.bairro
            newRegistro[i].localidade = endeCompl.localidade
            newRegistro[i].uf = endeCompl.uf
            newRegistro[i].complemento = endeJoin.complemento
            newRegistro[i].latitude = endeJoin.latitude
            newRegistro[i].longitude = endeJoin.longitude
          }
        }
      }

      return response.json(newRegistro)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new ParceiroService()
