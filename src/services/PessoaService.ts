import PessoaRepository from '../repositories/PessoaRepository'
import { getCustomRepository } from 'typeorm'

class ClienteService {
  async store (fkIdLoginUsuario: number, nomePessoa: string, ultimoNomePessoa: string, dataNascimento: Date, identidade: string, genero: string) {
    try {
      const repoPessoa = getCustomRepository(PessoaRepository)

      return await repoPessoa.save({
        fkIdLoginUsuario,
        nomePessoa,
        ultimoNomePessoa,
        dataNascimento,
        genero,
        identidade
      })
    } catch (err) {
      console.log(err.message)
    }
  }

  async update (idPessoa: number, fkIdLoginUsuario: number, nomePessoa: string, ultimoNomePessoa: string, dataNascimento: Date, identidade: string, genero: string) {
    try {
      const repoPessoa = getCustomRepository(PessoaRepository)

      return await repoPessoa.save({
        idPessoa,
        fkIdLoginUsuario,
        nomePessoa,
        ultimoNomePessoa,
        dataNascimento,
        genero,
        identidade
      })
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new ClienteService()
