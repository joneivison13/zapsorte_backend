import RegistroTituloRepository from '../repositories/RegistroTituloRepository'
import TituloRepository from '../repositories/TituloRepository'
import PessoaRepository from '../repositories/PessoaRepository'
import { getCustomRepository } from 'typeorm'
import { Request, Response } from 'express'

class RegistroTituloService {
  async store (request: Request, response: Response) {
    try {
      const { fkIdLoginUsuario, code } = request.body
      const repoT = getCustomRepository(TituloRepository)
      const repoR = getCustomRepository(RegistroTituloRepository)

      const titulo = await repoT.findOne({ where: { numeroTitulo: code } })

      if (!titulo) return response.status(404).send('Este título não foi cadastrado no ZapTroco.')

      const adquirido = await repoR.findOne({ where: { fkIdTitulo: titulo.idTitulo } })

      if (adquirido) return response.status(401).send('Este título ja foi adquirido')

      const registro = await repoR.save({
        fkIdLoginUsuario,
        fkIdTitulo: titulo.idTitulo,
        dataRegistro: new Date().toLocaleDateString().split('/').reverse().join('-')
      })

      return response.json(registro)
    } catch (err) {
      console.log(err.message)
    }
  }

  async getByUsuario (request: Request, response: Response) {
    try {
      const { fkIdLoginUsuario } = request.params
      const repoR = getCustomRepository(RegistroTituloRepository)

      const registro = await repoR.createQueryBuilder('registro_titulo')
        .innerJoinAndSelect('registro_titulo.titulo', 'titulo')
        .innerJoinAndSelect('titulo.parceiro', 'parceiro')
        .where(`fk_id_login_usuario = ${fkIdLoginUsuario}`)
        .getMany()

      response.json(registro)
    } catch (err) {
      console.log(err.message)
    }
  }

  async getGanhadores (request: Request, response: Response) {
    try {
      const repoR = getCustomRepository(RegistroTituloRepository)
      const repoP = getCustomRepository(PessoaRepository)

      var registro = await repoR.createQueryBuilder('registro_titulo')
        .innerJoinAndSelect('registro_titulo.titulo', 'titulo')
        .innerJoinAndSelect('titulo.parceiro', 'parceiro')
        .innerJoinAndSelect('registro_titulo.loginUsuario', 'loginUsuario')
        .where("registro_titulo.ganhador = 'S'")
        .getMany()

      var newRegistro = []
      if (registro.length > 0) {
        for (var i = 0; i < registro.length; i = i + 1) {
          const { nomePessoa } = await repoP.findOne({ fkIdLoginUsuario: registro[i].fkIdLoginUsuario })
          newRegistro[i] = registro[i]
          newRegistro[i].nome = nomePessoa
        }
      }

      response.json(newRegistro)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new RegistroTituloService()
