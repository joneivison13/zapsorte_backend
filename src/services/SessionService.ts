import { Request, Response } from 'express'
import LoginUsuarioRepository from '../repositories/LoginUsuarioRepository'
import UsuarioParceiroRepository from '../repositories/UsuarioParceiroRepository'
import PessoaRepository from '../repositories/PessoaRepository'
import { getCustomRepository } from 'typeorm'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'
import config from '../config'

class SessionService {
  async store (request: Request, response: Response) {
    try{
      const { cpfUsuario, password } = request.body

      const repo = getCustomRepository(LoginUsuarioRepository)
      const usuario: any = await repo.createQueryBuilder("login_usuario")
                                      .where("cpf = :cpf OR numero_celular = :numeroCelular")
                                      .setParameters({ cpf: cpfUsuario, numeroCelular: cpfUsuario })
                                      .getOne()
      
      if (!usuario) { return response.status(404).send('Usuário não encontrado') }
  
      const isPasswordCorrect = await bcrypt.compare(password, usuario.passwordUsuario)
  
      if (!isPasswordCorrect) { return response.status(401).send('Senha incorreta') }
  
      delete usuario.passwordUsuario
      delete usuario.criadoEm
      delete usuario.atualizadoEm
  
      const repoPessoa = getCustomRepository(PessoaRepository)
      const pessoa = await repoPessoa.findOne({ fkIdLoginUsuario: usuario.idLoginUsuario })
      usuario.pessoa = pessoa
  
      return response.json({
        usuario: usuario,
        token: jwt.sign(
          { idUsuario: usuario.idLoginUsuario },
          config.app.secret,
          {})
      })
    } catch (e) {

      if ( e.message === 'No metadata for "LoginUsuario" was found.' ) {
        return response.status(500).json({
          erro: true, 
          message: 'Favor alterar o arquivo ORMCONFIG na raiz do projeto.'
        });
      }

      console.log(e.message);
    }
  }

  async storeParceiro (request: Request, response: Response) {
    const { cnpj, password } = request.body

    const repo = getCustomRepository(UsuarioParceiroRepository)
    const usuario = await repo.findOne({ cpf: cnpj })

    if (!usuario) { return response.status(404).send('Usuário não encontrado') }

    const isPasswordCorrect = await bcrypt.compare(password, usuario.password)

    if (!isPasswordCorrect) { return response.status(401).send('Senha incorreta') }

    delete usuario.password
    delete usuario.criadoEm
    delete usuario.atualizadoEm

    return response.json({
      usuario: usuario,
      token: jwt.sign(
        { idUsuarioParceiro: usuario.idUsuarioParceiro },
        config.app.secret,
        { expiresIn: '7d' })
    })
  }

  async updatePassApp (request: Request, response: Response) {
    const { idLoginUsuario, /*password,*/ newPassword, confirmPassword } = request.body

    const repo = getCustomRepository(LoginUsuarioRepository)

    const usuario = await repo.findOne({idLoginUsuario: idLoginUsuario})

    if (!usuario) return response.status(401).send('Usuário não existe')

    if (newPassword !== confirmPassword) return response.status(401).send('As senhas não são iguais')

    // const isPasswordCorrect = await bcrypt.compare(password, usuario.passwordUsuario)

    // if (!isPasswordCorrect) { return response.status(401).send('Senha atual é incorreta') }

    const passwordHash = await bcrypt.hash(newPassword, Number(config.app.salt))

    await repo.update(idLoginUsuario, { passwordUsuario: passwordHash })

    return response.json({ message: 'Senha alterada com sucesso' })
  }

  async updatePassParceiro (request: Request, response: Response) {
    const { idUsuarioParceiro, password, newPassword, confirmPassword } = request.body

    const repo = getCustomRepository(UsuarioParceiroRepository)

    const usuario = await repo.findOne(idUsuarioParceiro)

    if (!usuario) return response.status(401).send('Usuário não existe')

    if (newPassword !== confirmPassword) return response.status(401).send('As senhas não são iguais')

    const isPasswordCorrect = await bcrypt.compare(password, usuario.password)

    if (!isPasswordCorrect) { return response.status(401).send('Senha atual incorreta') }

    const passwordHash = await bcrypt.hash(newPassword, Number(config.app.salt))

    await repo.update(idUsuarioParceiro, { password: passwordHash })

    return response.json({ message: 'Senha alterada com sucesso' })
  }

  async sessionRecuperarSenha (request: Request, response: Response) {
    const { numeroCelular } = request.body

    const repo = getCustomRepository(LoginUsuarioRepository)
    const usuario: any = await repo.findOne({ numeroCelular })

    if (!usuario) { return response.status(404).send('Usuário não encontrado') }

    delete usuario.passwordUsuario
    delete usuario.criadoEm
    delete usuario.atualizadoEm

    const repoPessoa = getCustomRepository(PessoaRepository)
    const pessoa = await repoPessoa.findOne({ fkIdLoginUsuario: usuario.idLoginUsuario })
    usuario.pessoa = pessoa

    return response.json({
      usuario: usuario,
      token: jwt.sign(
        { idUsuario: usuario.idLoginUsuario },
        config.app.secret,
        {})
    })
  }
}

export default new SessionService()
