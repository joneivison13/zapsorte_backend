import { Request, Response } from 'express'
import TituloRepository from '../repositories/TituloRepository'
import { getCustomRepository } from 'typeorm'

class TituloService {
  async store (request: Request, response: Response) {
    try {
      const { fkIdParceiro, numeroTitulo, dataLancamento, dataVencimento } = request.body

      const repo = getCustomRepository(TituloRepository)

      const titulo = await repo.save({
        fkIdParceiro, numeroTitulo, dataLancamento, dataVencimento
      })

      response.json(titulo)
    } catch (err) {
      console.log(err.message)
    }
  }

  async update (request: Request, response: Response) {
    try {
      const { idTitulo, numeroTitulo, dataLancamento, dataVencimento } = request.body

      const repo = getCustomRepository(TituloRepository)

      const { affected } = await repo.update(idTitulo, {
        numeroTitulo, dataLancamento, dataVencimento
      })

      response.json({ affected })
    } catch (err) {
      console.log(err.message)
    }
  }

  async get (request: Request, response: Response) {
    try {
      const repo = getCustomRepository(TituloRepository)
      const resp = await repo.find()

      response.json(resp)
    } catch (err) {
      console.log(err.message)
    }
  }

  async delete (request: Request, response: Response) {
    try {
      // const { idTitulo } = request.params
      // const repo = getCustomRepository(TituloRepository)
      // const repoUsuApp = getCustomRepository(UsuarioAppNivelAcessoRepository)
      // const repoUsuPar = getCustomRepository(UsuarioParceiroNivelAcessoRepository)

      // const existApp = await repoUsuApp.findOne({ fkIdNivelAcesso: Number(idNivelAcesso) })
      // if (existApp) { response.status(401).send('Existe usuário app associado a este nível de acesso') }

      // const existPar = await repoUsuPar.findOne({ fkIdNivelAcesso: Number(idNivelAcesso) })
      // if (existPar) { response.status(401).send('Existe usuário parceiro associado a este nível de acesso') }

      // const resp = await repo.delete(idNivelAcesso)

      // response.json(resp)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new TituloService()
