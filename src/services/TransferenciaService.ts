import TransferenciaRepository from '../repositories/TransferenciaRepository'
import MovimentacaoUsuariosRepository from '../repositories/MovimentacaoUsuariosRepository'
import { getCustomRepository } from 'typeorm'
import { Request, Response } from 'express'
import Mail from './MailService'

class TransferenciaService {
  async store (request: Request, response: Response) {
    try {
      const { fkIdBanco, 
        fkIdLoginUsuario, 
        contaCorrente, 
        agencia, 
        cpfDestino, 
        nomeCompletoDestino, 
        fkIdTipoMovimentacao, 
        descricaoMovimentacao, 
        valorMovimentacao, 
        nomeOrigem,
        cpf,
        banco} = request.body

      const repoMov = getCustomRepository(MovimentacaoUsuariosRepository)

      const registroMov = await repoMov.save({
        fkIdTipoMovimentacao,
        fkPagador: fkIdLoginUsuario, 
        fkRecebedor: null, 
        descricaoMovimentacao, 
        valorMovimentacao, 
        dataMovimentacao: new Date().toISOString().substr(0,10)
      })

      const repoTra = getCustomRepository(TransferenciaRepository)

      const registroTra = await repoTra.save({
        fkIdBanco, 
        fkIdLoginUsuario, 
        fkIdMovimentacaoUsuarios: registroMov.idMovimentacaoUsuarios, 
        contaCorrente, 
        agencia, 
        cpfDestino, 
        nomeCompletoDestino, 
      })

      let dataTime = new Date().toISOString().replace('T',' ')
      let data = dataTime.split(' ')[0].split('-').reverse().join('/')
      let time = dataTime.split(' ')[1].substr(0,8)
      
      const text = `Olá.<br /><br />
      Foi realizada uma <b>transferência</b> no ZapTroco. <br /><br />
      <b>De:</b> ${nomeOrigem} (CPF: ${cpf})<br />
      <b>Para:</b> ${nomeCompletoDestino} (CPF: ${cpfDestino})<br />
      <b>Banco:</b> ${banco.nomeBanco}<br />
      <b>Agência:</b> ${agencia}<br />
      <b>Conta:</b> ${contaCorrente}<br />
      <b>Data:</b>  ${data} ${time}<br />
      <b>Valor:</b> ${valorMovimentacao.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }).replace('-','')}<br /><br />
      Atenciosamente, <br />Equipe ZapTroco :)`

      Mail.send("ZapTroco: Transferência realizada!", "transferencia@zaptroco.com.br", text)
      
      return response.json({registroMov, registroTra})
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new TransferenciaService()

 