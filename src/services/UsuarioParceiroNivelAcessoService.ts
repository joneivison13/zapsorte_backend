import UsuarioParceiroNivelAcessoRepository from '../repositories/UsuarioParceiroNivelAcessoRepository'
import { getCustomRepository } from 'typeorm'

class UsuarioParceiroNivelAcessoService {
  async store (fkIdNivelAcesso: number, fkIdUsuarioParceiro: number) {
    try {
      const repo = getCustomRepository(UsuarioParceiroNivelAcessoRepository)
      await repo.delete({ fkIdUsuarioParceiro, fkIdNivelAcesso })

      return await repo.save({
        fkIdUsuarioParceiro,
        fkIdNivelAcesso
      })
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new UsuarioParceiroNivelAcessoService()
