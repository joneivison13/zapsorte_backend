import { Request, Response } from 'express'
import UsuarioParceiroRepository from '../repositories/UsuarioParceiroRepository'
import ParceiroService from '../services/ParceiroService'
import EnderecoService from '../services/EnderecoService'
import NivelAcessoService from '../services/NivelAcessoService'
import UsuarioParceiroNivelAcessoService from '../services/UsuarioParceiroNivelAcessoService'
import bcrypt from 'bcrypt'
import { getCustomRepository } from 'typeorm'
import config from '../config'

class UsuarioParceiroService {
  async store (request: Request, response: Response) {
    try {
      const {
        cnpj,
        password,
        razaoSocial,
        nomeFantasia,
        telefoneContato1,
        telefoneContato2,
        telefoneContato3,
        telefoneContato4,
        email1,
        email2,
        email3,
        email4,
        pessoaContato,
        celularContato,
        emailContato,
        idEndereco,
        cep,
        logradouro,
        complemento,
        bairro,
        localidade,
        uf,
        latitude,
        longitude
      } = request.body

      const repoUsuarioParceiro = getCustomRepository(
        UsuarioParceiroRepository
      )

      const exist = await repoUsuarioParceiro.findOne({
        where: [{ cnpj }]
      })

      if (exist) {
        return response.status(401).send('Usuário já existe')
      }

      const nivelAcesso = await NivelAcessoService.getDefaultParceiro()

      if (!nivelAcesso) {
        response.status(500).send('Nível de acesso "PARCEIRO" não encontrado')
      }

      const passwordHash = await bcrypt.hash(password, Number(config.app.salt))

      const usuarioParceiro = await repoUsuarioParceiro.save({
        cnpj,
        password: passwordHash,
        ativo: true
      })

      delete usuarioParceiro.password

      // let fkIdEndereco = idEndereco
      // if (!fkIdEndereco) {
      //   const endereco = await EnderecoService.store(
      //     cep,
      //     logradouro,
      //     complemento,
      //     bairro,
      //     localidade,
      //     uf
      //   )
      //   fkIdEndereco = endereco.idEndereco
      // }

      const parceiro = await ParceiroService.store(
        usuarioParceiro.idUsuarioParceiro,
        null,
        razaoSocial,
        nomeFantasia,
        telefoneContato1,
        telefoneContato2,
        telefoneContato3,
        telefoneContato4,
        email1,
        email2,
        email3,
        email4,
        pessoaContato,
        celularContato,
        emailContato,
        latitude,
        longitude
      )

      await UsuarioParceiroNivelAcessoService.store(
        nivelAcesso.idNivelAcesso,
        usuarioParceiro.idUsuarioParceiro
      )

      response.json({ usuarioParceiro, parceiro })
    } catch (err) {
      console.log(err.message)
    }
  }

  async getByCNPJ (request: Request, response: Response) {
    try {
      const { cnpj } = request.params
      const repo = getCustomRepository(UsuarioParceiroRepository)
      const usuario = await repo.findOne({ cpf: cnpj })

      if (usuario) {
        delete usuario.password
      }

      response.json(usuario)
    } catch (err) {
      console.log(err.message)
    }
  }
}

export default new UsuarioParceiroService()
