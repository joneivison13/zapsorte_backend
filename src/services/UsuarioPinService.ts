import UsuarioPinRepository from "../repositories/UsuarioPinRepository";
import LoginUsuarioRepository from "../repositories/LoginUsuarioRepository";
import { getCustomRepository } from "typeorm";
import { Request, Response } from "express";
import axios from "axios";
import app from "../config";

class UsuarioPinService {
  generatePin(): Number {
    const pin: Number = Math.floor(Math.random() * 9999);

    if (pin < 1000) return Number(String(pin) + "9");

    return pin;
  }

  async generate(request: Request, response: Response) {
    try {
      const { celular } = request.body;
      const repo = getCustomRepository(UsuarioPinRepository);
      const repoU = getCustomRepository(LoginUsuarioRepository);

      const exist = await repoU.find({ numeroCelular: celular });

      if (exist.length > 0) return response.json({ exist: true });

      const pinUsuario: Number = this.generatePin();

      const { data } = await axios({
        url:
          app.services.smsdev.url +
          `/v1/send?key=${
            app.services.smsdev.token
          }&type=9&number=${celular}&msg=${encodeURIComponent(
            `Z-${pinUsuario} é o seu código de ativação no ZapTroco.`
          )}&refer=${encodeURIComponent("ZapTroco")}`,
        method: "GET",
        headers: {},
      });

      console.log("data", data);

      const registro = await repo.save({
        fkIdLoginUsuario: null,
        pinUsuario: String(pinUsuario),
        celular,
      });

      response.json({ registro, sms: data });
    } catch (err) {
      console.log(err);
      response.json({
        err,
        msg: "Houve uma imprevisto no envio do SMS, tente novamente mais tarde...",
      });
    }
  }

  async validate(request: Request, response: Response) {
    try {
      const { pinUsuario, celular } = request.body;
      const repo = getCustomRepository(UsuarioPinRepository);

      const registro = await repo.find({ where: { celular, pinUsuario } });

      response.json({ valid: !!registro.length });
    } catch (err) {
      console.log(err.message);
    }
  }

  async generateRecuperarSenha(request: Request, response: Response) {
    try {
      const { celular, cpf } = request.body;
      console.log(request.body);
      const repo = getCustomRepository(UsuarioPinRepository);
      const repoU = getCustomRepository(LoginUsuarioRepository);

      let exist = null;
      if (celular) exist = await repoU.findOne({ numeroCelular: celular });

      if (cpf) exist = await repoU.findOne({ cpf });

      if (!exist) return response.json({ notExist: true });

      const pinUsuario: Number = this.generatePin();

      const { data } = await axios({
        url:
          app.services.smsdev.url +
          `/v1/send?key=${app.services.smsdev.token}&type=9&number=${
            exist.numeroCelular
          }&msg=${encodeURIComponent(
            `Z-${pinUsuario} é o seu código de ativação no ZapTroco.`
          )}&refer=${encodeURIComponent("ZapTroco")}`,
        method: "GET",
        headers: {},
      });

      const registro: any = await repo.save({
        fkIdLoginUsuario: exist.idLoginUsuario,
        pinUsuario: String(pinUsuario),
        celular: exist.numeroCelular,
      });

      registro.cpf = exist.cpf;

      response.json({ registro, sms: data });
    } catch (err) {
      console.log(err);
      response.json({
        err,
        msg: "Houve uma imprevisto no envio do SMS, tente novamente mais tarde...",
      });
    }
  }
}

export default new UsuarioPinService();
